class Email {
  constructor(email) {
    if(email && email.length <= 100 && this.validate(email)) {
      this.email = email;
      Object.freeze(this);
    } else {
      throw Error('Invalid Input');
    }
  }

  validate(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
}

module.exports = { Email };
