class Name {
  constructor(name) {
    if(name && name.length <= 100) {
      this.name = name;
      Object.freeze(this);
    } else {
      throw Error('Invalid Input');
    }
  }
}

module.exports = { Name };
