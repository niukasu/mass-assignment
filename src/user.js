const { Email } = require('./email');
const { Name } = require('./name');

class User {
    constructor(name, email) {
        this.email = new Email(email);
        this.name = new Name(name);
        this.id = auth_user_id();
        this.role = "user";
        Object.freeze(this);
    }
}

// A psudo authenticator
// returns authenticated user ID

function auth_user_id() {
    return 2;
}



module.exports = { User, auth_user_id };
